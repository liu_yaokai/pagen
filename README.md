# ParserE

ParserE is a C program library-framework to generate 
a parser which can be integrated into a larger 
project as a submodule.

It is constructed with 3 parts.

- Rule: this part provides a framework to compose a rule. User uses
the framework to write rules and compose a rule pool, which will 
be used in **Parse** part.

- Parse: this part parse sources to target objects. When parsing, 
for every loop, program will filter which rule is applied, 
construct the source objects to a target object, and store target
object to an array. Finally, return the target object array.

- Message: this part provides a list of report messages. However, 
these messages are just for parse instead of rule. If users want to
use some log system to track errors, they can write into rules' 
`detector` and `constructor`, which are the real function to check the
source objects and construct target objects.

### How to use it

Users need maintain three parts:

1. the parse rules.
2. what are the source objects. the struct of source objects, functions to
operate source objects.
3. what are the target objects. the struct of target objects, functions to
operate target objects.

