/*
MIT License

Copyright (c) 2022 Yaokai Liu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

//
// Created by Yaokai Liu on 2022/6/18.
//

#ifndef PARSER_E_RULE_H
#define PARSER_E_RULE_H

#include "def.h"
/*
 * detector:
 *   params:
 *      @ sources:  pointer refers to head of source objects
 *   return:
 *      how many source objects has been counted
 */
typedef size_t (* detector)(void * sources);

/*
 * constructor:
 *
 *      params:
 *          @ sources:  pointer refers to head of source objects
 *          @ count:    how many source objects should be parsed
 *      return:
 *          pointer refers to a target object
 *
 * The pointer refers to the target object should be stored into
 * a pointer array.
 */
typedef void * (* constructor)(void * sources, size_t count);

/*
 * rule_kind:
 *      An enum type describes kinds of rules.
 *
 *      @ ignore_rule:  means the rule describes ignored objects.
 *                      if a rule is 'ignore_rule', then the target
 *                      of this rule will be ignored.
 *
 *     @ literal_rule:  means the rule describes a literal thing.
 *
 *     @ struct_rule:   means the rule describes a structure.
 */
typedef enum {
    ignore_rule = 0,
    literal_rule = 1,
    struct_rule = 2
} rule_kind;


/* Rule_Struct:
 *
 *      A struct to keep rule's attribute.
 *
 *      attributes:
 *          @ rule_id:      id of this rule, it may be used to refer back to this rule.
 *
 *          @ kind:         which kind this rule is. you can loop up for 'rule_kind' type.
 *
 *          @ name:         name of this rule, it may be used for debug and log.
 *
 *          @ annotate:     annotate of this rule, to explain how is this rule,
 *                          it may be used for debug and log.
 *
 *          @ detector:     detector of this rule, check if this rule complies given sources,
 *                          is a function with 'detector' type.
 *
 *          @ constructor:  constructor of this rule, construct a target object described by this rule,
 *                          is a function with 'constructor' type.
*/
typedef struct {
    id              rule_id;
    rule_kind       kind;
    char *          name;
    char *          annotate;
    detector        detector;
    constructor     constructor;
} Rule_Struct;

/*
 * Rule:
 *      A pointer refers to a 'Rule_Struct'.
 *
 * Rule_Struct:
 *
 *      A struct to keep rule's attribute.
 *
 *      attributes:
 *          @ rule_id:      id of this rule, it may be used to refer back to this rule.
 *
 *          @ kind:         which kind this rule is. you can loop up for 'rule_kind' type.
 *
 *          @ name:         name of this rule, it may be used for debug and log.
 *
 *          @ annotate:     annotate of this rule, to explain how is this rule,
 *                          it may be used for debug and log.
 *
 *          @ detector:     detector of this rule, check if this rule complies given sources,
 *                          is a function with 'detector' type.
 *
 *          @ constructor:  constructor of this rule, construct a target object described by this rule,
 *                          is a function with 'constructor' type.
*/
typedef Rule_Struct * Rule;


/*
 * RulePool_Struct:
 *      An array with size to store all rules.
 *      attributes:
 *          @ size:     how many rules in this array.
 *          @ nonend:   detect if current source object is not the end of source.
 *          @ pool:     the array.
 */
typedef struct {
    size_t      size;
    detector    nonend;
    Rule        pool[];
} RulePool_Struct;

/*
 * RulePool:
 *      A pointer refers to a 'RulePool_struct'.
 * RulePool_Struct:
 *      An array with size to store all rules.
 *      attributes:
 *          @ size:  how many rules in this array.
 *          @ pool:  the array.
 */
typedef const RulePool_Struct * RulePool;

#endif //PARSER_E_RULE_H
